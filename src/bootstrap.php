<?php 

define('ROOTDIR', dirname(dirname((__FILE__))));

require_once 'config/config.php';

require_once ROOTDIR . '\vendor\autoload.php';

include( ROOTDIR .'\include\utils.php');


require_once 'classes/App.php';
require_once 'classes/Controller.php';


