<?php

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">

	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	
	<link rel="stylesheet" href="<?= BASEURL ?>static/styles.css">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
	  <div class="container">
	      <a class="navbar-brand" href="#">PHP SENIOR TEST</a>
	      
	      <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
	        <div class="navbar-nav">
	          <a class="nav-item nav-link" href="<?= BASEURL ?>Report/three_pointers">Report 1</a>
	          <a class="nav-item nav-link" href="<?= BASEURL ?>Report/three_pointers_team">Report 2</a>
	        </div>
	      </div>
	  </div>
	</nav>

	<div id="container">

	    <div class="row">
	        <div class="col-lg-12 pt-3">

	        	<div class="bg-image"></div>

				<div class="bg-text">
				  <h2 class="text-light"><?= $data->note; ?></h2>
				  <br/>
				  <h3 class="text-warning" style="float: left;"><?= $data->title; ?></h3>
				  <div style="clear: both;"></div>
				  <p><?= $data->description; ?></p>
				</div>
	        </div>
	    </div>

	    <div style="margin-top:25px;"></div>

	    <div class="row justify-content-md-center">
			<div class="col-10 col-auto">
			
				<table class="table table-bordered table-dark">
				  <thead>
				    <tr>
				      <th scope="col">Team</th>
				      <th scope="col">Accuracy</th>
				      <th scope="col">Total 3Pts Made</th>
				      <th scope="col">#Player Contributed</th>
				      <th scope="col">#Player Attempted</th>
				      <th scope="col">#Failed Attempts</th>

				    </tr>
				  </thead>
				  <tbody>
				    <?php
				        foreach($data->nodeData as $row) {
				        	$html = '<tr>';

				            $html .= "<th scope='row' class=''>" . $row['name'] . "</th>";
				            $html .= "<td>" . round($row['total_team_accuracy'],2) . "%</td>";
				            $html .= "<td>" . $row['total_team_pts'] . "</td>";
				            $html .= "<td>" . $row['contributing_players'] . "</td>";
				            $html .= "<td>" . $row['attempting_players'] . "</td>";
				            $html .= "<td>" . $row['failed_3pt'] . "</td>";

				            $html .= '</tr>';

				        	echo $html;
				        }
			        ?>

				  </tbody>
				</table>

			</div>
		</div>
	</div>

	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>    


</body>
</html>


