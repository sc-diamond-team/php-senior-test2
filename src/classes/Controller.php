<?php 

use Illuminate\Support;  // https://laravel.com/docs/5.8/collections - provides the collect methods & collections class
use LSS\Array2Xml;

class Controller {

    private $args;

    public function view($view, $data = [])
    {
        require_once ROOTDIR . '/src/views/' . $view . '.php';
    }

    public function model($model)
    {
        require_once ROOTDIR . '/src/models/' . $model . '.php';
        return new $model;
    }

    public function export($type, $format, $args = null) {

        $this->args = $args;

        $data = [];

        require_once 'Exporter.php';

        $exporter = new Exporter();
        switch ($type) {
            case 'playerstats':
                $searchArgs = ['player', 'playerId', 'team', 'position', 'country'];
                $search = $this->args->filter(function($value, $key) use ($searchArgs) {
                    return in_array($key, $searchArgs);
                });
                $data = $exporter->getPlayerStats($search);
                break;
            case 'players':
                $searchArgs = ['player', 'playerId', 'team', 'position', 'country'];
                $search = $this->args->filter(function($value, $key) use ($searchArgs) {
                    return in_array($key, $searchArgs);
                });
                $data = $exporter->getPlayers($search);
                break;
        }
        if (!$data) {
            exit("Error: No data found!");
        }
        return $exporter->format($data, $format);
    }
}