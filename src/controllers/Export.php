<?php


use Illuminate\Support;

class Export extends Controller {

	
	public function index() {

		$args = collect($_REQUEST);

		$format = $args->pull('format') ?: 'html';
		
		$type = $args->pull('type');
		
		if (!$type) {

		    exit('Please specify a type');
		
		}

		echo $this->export($type, $format, $args);


	}





}