<?php

use Illuminate\Support;

class Report extends Controller {

    private $accuracy = 35; // 35
    private $min_age = 30; // 35

    public function __construct() {

    }

    public function index() {
    
    }

    public function three_pointers() {

        $sql = "
            SELECT p.player_id, r.name AS player_name, t.name AS team_name, p.age, r.number, r.pos, p.3pt,
            ((p.3pt / (p.3pt_attempted)) * 100) AS percentage,
            CONCAT(FORMAT(IF(p.3pt_attempted=0,0,(p.3pt * 100.0)/p.3pt_attempted),2),'%') AS FORMATTED
            FROM player_totals AS p
            INNER JOIN roster AS r ON r.id = p.player_id
            INNER JOIN team AS t ON t.code = r.team_code
            WHERE
            p.age > ". $this->min_age ." AND 
            ((p.3pt / (p.3pt_attempted)) * 100) > ". $this->accuracy;

        $data = query($sql) ?: [];

        if($data) {
            $data_c = array_column($data, 'percentage');
            array_multisort($data_c, SORT_DESC, $data);
        }

        $full_data = (object) array(
            'title' => 'Report 1 - Best 3pt Shooters',
            'description' => 'Produce a query that reports on the best 3pt shooters in the database that are older than 30 years old. Only retrieve data for players who have shot 3-pointers at greater accuracy than 35%.',
            'note' => 'Rank the data by the players with the best % accuracy first.',
            'nodeData' => $data
        );

        // dd($data , $data_c);
        
        $this->view('report/report1', $full_data);
    }

    public function three_pointers_team() {

        $sql = "
            SELECT t.name, 
            ((SUM(p.3pt) / (SUM(p.3pt_attempted))) * 100) AS total_team_accuracy
            ,SUM(p.3pt) AS total_team_pts
            ,COUNT(DISTINCT IF(p.3pt > 0,
                        p.player_id,
                        NULL)) AS contributing_players
            ,COUNT(DISTINCT IF(p.3pt_attempted > 0,
                        p.player_id,
                        NULL)) AS attempting_players
            ,COUNT(DISTINCT IF(p.3pt <= 0 AND p.3pt_attempted > 1,
                        p.player_id,
                        NULL)) AS failed_3pt
            FROM player_totals AS p
            INNER JOIN roster AS r ON r.id = p.player_id
            INNER JOIN team AS t ON t.code = r.team_code
            GROUP BY t.name
        ";

        $data = query($sql) ?: [];

        if($data) {
            $data_c = array_column($data, 'total_team_accuracy');
            array_multisort($data_c, SORT_DESC, $data);
        }

        $full_data = (object) array(
            'title' => 'Report 2 - Best 3pt Shooting Teams',
            'description' => 'You should be able to retrieve all data in a single query, without subqueries. Put the most accurate 3pt teams first.',
            'note' => 'Produce a query that reports on the best 3pt shooting teams.',
            'nodeData' => $data
        );

        // dd($data , $data_c);
        
        $this->view('report/report2', $full_data);
    }
}